# Reactive CRUD REST API with Spring WebFLux

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/reactive-crud-rest-api-with-spring-webflux.git`
2. Go inside the folder: `cd reactive-crud-rest-api-with-spring-webflux`
3. Make sure your MongoDB is running well.
4. Run the application: `mvn clean spring-boot:run`

Article Source
--> [Here](https://javatechonline.com/how-to-develop-a-reactive-crud-rest-api-with-spring-webflux/?unapproved=373&moderation-hash=e01980f69df382b91e2a9a05238da5f3#comment-373)

