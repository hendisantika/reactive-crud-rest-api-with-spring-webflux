package com.hendisantika.reactivecrudrestapiwithspringwebflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveCrudRestApiWithSpringWebfluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveCrudRestApiWithSpringWebfluxApplication.class, args);
    }

}
