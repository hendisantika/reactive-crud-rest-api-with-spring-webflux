package com.hendisantika.reactivecrudrestapiwithspringwebflux.client;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 13.59
 */
@Component
@Log4j2
public class DeleteInvoiceRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        WebClient client = WebClient.create("http://localhost:8080");
        Mono<Void> mono = client
                .delete()
                .uri("/invoices/3")
                .retrieve()
                .bodyToMono(Void.class);
        mono.subscribe(x -> log.info(x));
        log.info("Invoice Deleted!");
    }

}
