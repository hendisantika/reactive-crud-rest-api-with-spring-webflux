package com.hendisantika.reactivecrudrestapiwithspringwebflux.client;

import com.hendisantika.reactivecrudrestapiwithspringwebflux.model.Invoice;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 13.51
 */
@Component
@Log4j2
public class GetAllInvoicesRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        WebClient client = WebClient.create("http://localhost:8080");
        Flux<Invoice> flux = client
                .get()
                .uri("/invoices")
                .retrieve()
                .bodyToFlux(Invoice.class);
        flux.doOnNext(x -> log.info(x)).blockLast();
    }


}
