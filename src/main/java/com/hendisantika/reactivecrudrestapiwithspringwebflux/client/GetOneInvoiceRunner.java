package com.hendisantika.reactivecrudrestapiwithspringwebflux.client;

import com.hendisantika.reactivecrudrestapiwithspringwebflux.model.Invoice;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 13.56
 */

@Component
@Log4j2
public class GetOneInvoiceRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        WebClient client = WebClient.create("http://localhost:8080");
        Mono<Invoice> mono = client
                .get()
                .uri("/invoices/3")
                .retrieve()
                .bodyToMono(Invoice.class);
        mono.subscribe(x -> log.info(x));
    }

}