package com.hendisantika.reactivecrudrestapiwithspringwebflux.controller;

import com.hendisantika.reactivecrudrestapiwithspringwebflux.model.Invoice;
import com.hendisantika.reactivecrudrestapiwithspringwebflux.service.IInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 11.17
 */
@RestController
@RequestMapping("/invoices")
public class InvoiceController {
    @Autowired
    private IInvoiceService service;

    @PostMapping
    public Mono<Invoice> saveOneInvoice(@RequestBody Invoice invoice) {
        return service.saveInvoice(invoice);
        /*
        for Mono<String>
         service.saveInvoice(invoice);
         return Mono.just("Invoice saved" + invoice.getId());
         */
    }

    @GetMapping
    public Flux<Invoice> getAllInvoices() {
        return service.findAllInvoices();
    }

    @GetMapping("/{id}")
    public Mono<Invoice> getOneInvoice(@PathVariable Integer id) {
        return service.getOneInvoice(id);
    }

    @DeleteMapping("/{id}")
    public Mono<String> deleteInvoice(@PathVariable Integer id) {
        service.deleteInvoice(id);
        return Mono.just("Invoice with id: " + id + " deleted !");
    }
}
