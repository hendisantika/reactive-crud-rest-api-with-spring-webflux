package com.hendisantika.reactivecrudrestapiwithspringwebflux.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 11.12
 */
@Data
@Document
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

    @Id
    private Integer id;
    private String name;
    private String number;
    private Double amount;
}
