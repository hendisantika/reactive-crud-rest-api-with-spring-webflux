package com.hendisantika.reactivecrudrestapiwithspringwebflux.repository;

import com.hendisantika.reactivecrudrestapiwithspringwebflux.model.Invoice;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 11.12
 */
public interface InvoiceRepository extends ReactiveMongoRepository<Invoice, Integer> {

}
