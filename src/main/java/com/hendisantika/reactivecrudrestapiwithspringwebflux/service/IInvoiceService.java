package com.hendisantika.reactivecrudrestapiwithspringwebflux.service;

import com.hendisantika.reactivecrudrestapiwithspringwebflux.model.Invoice;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-crud-rest-api-with-spring-webflux
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/02/21
 * Time: 11.14
 */
public interface IInvoiceService {
    Mono<Invoice> saveInvoice(Invoice invoice);

    Flux<Invoice> findAllInvoices();

    Mono<Invoice> getOneInvoice(Integer id);

    Mono<Void> deleteInvoice(Integer id);
}
